extends Node

var current_square = null
var current_player = "X"
var current_state = "playing"

func _ready():

	pass


func _process(delta):
	if (current_state == "playing"):
		place_icon(current_square,current_player)
	if (current_state == "game over"):
		$Music.stop()
		$Fanfare.play()
		display_winner(current_state,current_player)
		current_state = "reset"
	if (current_state == "tie"):
		$Music.stop()
		$Tie.play()
		display_winner(current_state,current_player)
		current_state = "reset"
	if (current_state == "reset"):
		if Input.is_action_just_pressed("reset_game"):
			$Fanfare.stop()
			$Tie.stop()
			get_tree().reload_current_scene()

func _Area_Entered(name):
	current_square = name

func place_icon(square,player):
	if Input.is_action_just_pressed("left_click"):
		var node_vis_X  = "Board/%s/CollisionShape2D/X" % [square]
		var node_vis_O  = "Board/%s/CollisionShape2D/O" % [square]
		var node_name   = "Board/%s/CollisionShape2D/%s" % [square,player]
		
		if ( !(get_node(node_vis_X).is_visible()) and !(get_node(node_vis_O).is_visible()) ):
			$Click.play()
			get_node(node_name).set_visible(true)
			check_for_win()
			if (current_state == "playing"):
				if player == "X":
					current_player = "O"
				elif player == "O":
					current_player = "X"

func check_for_win():
	var tlx = get_node("Board/TopLeft/CollisionShape2D/X").is_visible()
	var mlx = get_node("Board/MidLeft/CollisionShape2D/X").is_visible()
	var blx = get_node("Board/BotLeft/CollisionShape2D/X").is_visible()
	var tmx = get_node("Board/TopMid/CollisionShape2D/X").is_visible()
	var mmx = get_node("Board/MidMid/CollisionShape2D/X").is_visible()
	var bmx = get_node("Board/BotMid/CollisionShape2D/X").is_visible()
	var trx = get_node("Board/TopRight/CollisionShape2D/X").is_visible()
	var mrx = get_node("Board/MidRight/CollisionShape2D/X").is_visible()
	var brx = get_node("Board/BotRight/CollisionShape2D/X").is_visible()
	var tlo = get_node("Board/TopLeft/CollisionShape2D/O").is_visible()
	var mlo = get_node("Board/MidLeft/CollisionShape2D/O").is_visible()
	var blo = get_node("Board/BotLeft/CollisionShape2D/O").is_visible()
	var tmo = get_node("Board/TopMid/CollisionShape2D/O").is_visible()
	var mmo = get_node("Board/MidMid/CollisionShape2D/O").is_visible()
	var bmo = get_node("Board/BotMid/CollisionShape2D/O").is_visible()
	var tro = get_node("Board/TopRight/CollisionShape2D/O").is_visible()
	var mro = get_node("Board/MidRight/CollisionShape2D/O").is_visible()
	var bro = get_node("Board/BotRight/CollisionShape2D/O").is_visible()
	# Win Condition 1
	if ( (tlx and mlx and blx) or (tlo and mlo and blo) ):
		current_state = "game over"
	# Win Condition 2
	elif ( (tmx and mmx and bmx) or (tmo and mmo and bmo) ):
		current_state = "game over"
	# Win Condition 3
	elif ( (trx and mrx and brx) or (tro and mro and bro) ):
		current_state = "game over"
	# Win Condition 4
	elif ( (tlx and tmx and trx) or (tlo and tmo and tro) ):
		current_state = "game over"
	# Win Condition 5
	elif ( (mlx and mmx and mrx) or (mlo and mmo and mro) ):
		current_state = "game over"
	# Win Condition 6
	elif ( (blx and bmx and brx) or (blo and bmo and bro) ):
		current_state = "game over"
	# Win Condition 7
	elif ( (tlx and mmx and brx) or (tlo and mmo and bro) ):
		current_state = "game over"
	# Win Condition 8
	elif ( (trx and mmx and blx) or (tro and mmo and blo) ):
		current_state = "game over"
	# Tie Condition
	elif ( (tlx or tlo) and (tmx or tmo) and (trx or tro) and (mlx or mlo) and (mmx or mmo) and (mrx or mro) and (blx or blo) and (bmx or bmo) and (brx or bro) ):
		current_state = "tie"

func display_winner(state,player):
	$Board.set_visible(false)
	if (state == "tie"):
		$Tied.set_visible(true)
	else:
		var winner = "Wins/%s" % [player]
		$Wins.set_visible(true)
		get_node(winner).set_visible(true)
	$Reset.set_visible(true)